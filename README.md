# orbi_devices
This python script pulls the list of connected devices from your Orbi mesh. Optionally send list of devices to Splunk HEC.

## Pre-requisites

* Orbi router firmware version `V2.7.4.24`
* Install `Python 3.10` from https://www.python.org/downloads/
* install environment and dependencies:
  * Create virtual environment: `python -m venv .venv`
  * If using Linux run: `source .venv/bin/activate`
  * If using Windows run: `.venv\Scripts\activate.bat` 
  * Install requirements: `pip install -r requirements.txt`

## Usage
`python get_connected_devices.py [-h] [-s SPLUNK_IP] [-t TOKEN_AUTH] [-c CSV] -u USERNAME -p PASSWORD -i IP_ADDRESS`

This python script pulls the list of devices connected to you Orbi mesh router.
Examples:
--------------------------------
    Displays the list of devices to the screen: 
      `python3 get_connected_devices.py -u admin -p password -i 192.168.1.1`
--------------------------------    
    Displays the list of devices to the screen and send to Splunk HEC:
      `python3 get_connected_devices.py -u admin -p password -i 192.168.1.1 -s 192.168.1.2 -a abcdefg-5555-7777-9999-hijklmnopqrst`
--------------------------------

```commandline
options:
  -h, --help            show this help message and exit
  -s SPLUNK_IP, --splunk-ip SPLUNK_IP
                        Splunk HEC IP address
  -t TOKEN_AUTH, --token-auth TOKEN_AUTH
                        Splunk HEC authentication token
  -u USERNAME, --username USERNAME
                        Orbi router username
  -p PASSWORD, --password PASSWORD
                        Orbi router password
  -i IP_ADDRESS, --ip-address IP_ADDRESS
                        Orbi router IP address
```

## Output

```commandline
user@host .scripts % python3 get_connected_devices.py -u admin -p password -i 192.168.1.1 -s 192.168.1.2 -a abcdefg-5555-7777-9999-hijklmnopqrst 
2022-05-29 13:58:21,605 [INFO] Trying to login to Orbi mesh router
2022-05-29 13:58:22,596 [INFO] Devices connected to Orbi mesh:
2022-05-29 13:58:22,596 [INFO] (name=orbisat1) (ip=192.168.1.251) (node=orbimain) (type=5G)
2022-05-29 13:58:22,596 [INFO] (name=orbisat2) (ip=192.168.1.250) (node=orbimain) (type=5G)
2022-05-29 13:58:22,597 [INFO] (name=Wyze) (ip=192.168.1.181) (node=orbisat1) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=RING-ABC) (ip=192.168.1.204) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=ADI Engineering) (ip=192.168.1.254) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Ring Solutions) (ip=192.168.1.207) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=BLINK_SYNC_MODULE) (ip=192.168.1.238) (node=orbisat1) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=Apple-Apple TV) (ip=192.168.1.212) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=1) (ip=192.168.1.180) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=Netgear-ProSAFE Plus) (ip=192.168.1.195) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=LAPTOP1) (ip=192.168.1.201) (node=orbimain) (type=5G)
2022-05-29 13:58:22,597 [INFO] (name=iRobot-Roomba) (ip=192.168.1.219) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=BLINK_SYNC_MODULE) (ip=192.168.1.237) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=Amazon) (ip=192.168.1.200) (node=orbisat1) (type=5G)
2022-05-29 13:58:22,597 [INFO] (name=LAPTOP2) (ip=192.168.1.220) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Apple-iMac) (ip=192.168.1.253) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Besstar Tech-GK41) (ip=192.168.1.199) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=SENSI-ABC) (ip=192.168.1.197) (node=orbisat1) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=Synology1) (ip=192.168.1.192) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=LAPTOP3) (ip=192.168.1.188) (node=orbisat2) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Sony-PlayStation) (ip=192.168.1.185) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Ring-Video Doorbell) (ip=192.168.1.178) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=Ring-Stick Up Cam) (ip=192.168.1.172) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=LG-OLED) (ip=192.168.1.209) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Amcrest) (ip=192.168.1.231) (node=orbisat1) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Asus-RT (ip=192.168.1.193) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=TP-Link-Smart Wi-Fi Plug Lite) (ip=192.168.1.244) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=TP-Link-Smart Wi-Fi Plug Lite) (ip=192.168.1.243) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=TP-Link-Smart Wi-Fi Plug Lite) (ip=192.168.1.232) (node=orbisat1) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=TP-Link-Smart Wi-Fi Plug Lite) (ip=192.168.1.245) (node=orbisat1) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=DEV-ABC) (ip=192.168.1.233) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] (name=Apple-iPhone) (ip=192.168.1.187) (node=Orbi Satellite) (type=5G)
2022-05-29 13:58:22,597 [INFO] (name=Synology2) (ip=192.168.1.242) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=MACBOOKPROHUB) (ip=192.168.1.174) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Apple-MacBook) (ip=192.168.1.186) (node=orbimain) (type=wired)
2022-05-29 13:58:22,597 [INFO] (name=Ring) (ip=192.168.1.205) (node=orbimain) (type=2.4G)
2022-05-29 13:58:22,597 [INFO] Sending to Splunk HTTP event collector https://192.168.1.2:8088/services/collector/event
2022-05-29 13:58:22,618 [INFO] Posted device list to Splunk HEC http status=200
2022-05-29 13:58:22,619 [INFO] Finished execution.
```