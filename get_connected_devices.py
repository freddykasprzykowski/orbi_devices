"""
Pulls list of devices connected to Orbi mesh router
"""
import requests
from requests.auth import HTTPBasicAuth
import time
import urllib3
import json
import argparse
import logging
import textwrap
import sys


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[logging.StreamHandler()],
)
logger = logging.getLogger()
parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=textwrap.dedent(
        """\
        This python script pulls the list of devices connected to you Orbi mesh router.
        Examples:
        --------------------------------
            Displays the list of devices to the screen: 
              `python3 get_connected_devices.py -u admin -p password -i 192.168.1.1`
        --------------------------------    
            Displays the list of devices to the screen and send to Splunk HEC:
              `python3 get_connected_devices.py -u admin -p password -i 192.168.1.1 -s 192.168.1.2 -a abcdefg-5555-7777-9999-hijklmnopqrst`
        --------------------------------
        """
    ),
)
parser.add_argument("-s", "--splunk-ip", help="Splunk HEC IP address")
parser.add_argument("-t", "--token-auth", help="Splunk HEC authentication token")
parser.add_argument("-u", "--username", help="Orbi router username", required=True)
parser.add_argument("-p", "--password", help="Orbi router password", required=True)
parser.add_argument("-i", "--ip-address", help="Orbi router IP address", required=True)
args = parser.parse_args()


def get_connected_devices(url, admin, passwd):
    r = requests.get(url, auth=HTTPBasicAuth(admin, passwd), verify=False)
    devices = r.text.splitlines()[1][7:]
    return r.status_code, devices


def post_splunk_http_collector(url, token, json_text):
    epoch = str(int(time.time()))
    json_text = '{"time": "' + epoch + '", "event": ' + json_text + "}"
    blob_json = json.loads(json_text)
    splunk_header = json.loads('{"Authorization" : "Splunk ' + token + '"}')
    result = requests.post(url=url, headers=splunk_header, json=blob_json, verify=False)
    return result


if __name__ == "__main__":
    urllib3.disable_warnings()
    orbi_ip = args.ip_address
    orbi_url = "https://" + orbi_ip + "/DEV_device_info.htm"
    orbi_admin = args.username
    orbi_password = args.password
    logger.info("Trying to retrieve connected devices to Orbi mesh")
    http_response = 0
    retries = 5
    while http_response != 200 and retries != 0:
        retries = retries - 1
        if orbi_password and orbi_admin and orbi_ip:
            http_response, devices_text = get_connected_devices(
                orbi_url, orbi_admin, orbi_password
            )
        else:
            logger.error(
                "Missing or incorrect IP address, username, or password for the Orbi router"
            )
            logger.error(
                f"you have tried with IP address={orbi_ip}, username={orbi_admin}, and password={orbi_password}"
            )
            sys.exit()
        if http_response == 401:
            logger.error(f"Failed to connect to Orbi router")
            logger.error(f"Got response code {http_response}")
            if retries == 0:
                logger.error(f"Try again with the correct username, password, and ip address.")
            else:
                logger.error(f"Trying again retries={retries}.")
        elif http_response == 200:
            logger.info("--------------------------------Devices connected to Orbi mesh-------------------------------")
            device_list = json.loads(devices_text)
            for device in device_list:
                logger.info(
                    f"(name={device['name']}) (ip={device['ip']}) (node={device['conn_orbi_name']}) (type={device['contype']})"
                )
            logger.info("---------------------------------Connected devices retrieved---------------------------------")
        else:
            logger.error(f"Ending, something very wrong happened")
            logger.error(f"Expecting 200 or 401, got {http_response}")
            logger.error(f"Report this error opening an issue in Gitlab")
            sys.exit()
        orbi_splunk = args.splunk_ip
        orbi_hec = args.token_auth
        if http_response == 200 and (orbi_splunk and orbi_hec):
            # Send results to Splunk HTTP event collector with event type = _json
            splunk_host = f"{orbi_splunk}:8088"
            # URL of the Splunk HTTP event collector
            splunk_url = "https://" + splunk_host + "/services/collector/event"
            # Read Splunk HTTP event collector auth token from file .splunk
            # Token in file is an example from
            # https://docs.splunk.com/Documentation/Splunk/8.1.1/Data/HTTPEventCollectortokenmanagement
            logger.info(f"Sending to Splunk HTTP event collector {splunk_url}")
            response = post_splunk_http_collector(splunk_url, orbi_hec, devices_text)
            if response.status_code == 200:
                logger.info(
                    f"Posted device list to Splunk HEC http status={response.status_code}"
                )
            else:
                logger.error(
                    f"Failed to post to Splunk HEC: http response code {response.status_code}"
                )
