# Security Policy

## Supported Versions

| Version | Supported          |
| ------- | ------------------ |
| 0.0.1   | :white_check_mark: |


## Reporting a Vulnerability

Tell me the module and vulnerability found
