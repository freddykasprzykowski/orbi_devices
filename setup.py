from setuptools import setup, find_packages

with open("requirements.txt") as f:
    required = f.read().splitlines()

setup(
    name="orbi_devices",
    version="0.0.1",
    url="https://github.com/scurvylogs/orbi_devices",
    description=("pull device list from an Orbi router"),
    maintainer="Freddy Kasprzykowski",
    maintainer_email="78245616+scurvylogs@users.noreply.github.com",
    license="MIT",
    packages=find_packages(),
    install_requires=required,
    classifiers=[
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
)
